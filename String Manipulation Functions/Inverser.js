function inverserChaine(chaine) {
    let chaineInverse = "";
    
    // Parcourir la chaîne de caractères de la fin vers le début
    for (let i = chaine.length - 1; i >= 0; i--) {
      chaineInverse += chaine[i];
    }
    
    return chaineInverse;
  }
  
  // Exemple d'utilisation
  const chaineOriginale = "Bonjour";
  const chaineInversee = inverserChaine(chaineOriginale);
  console.log(chaineInversee); // Affiche "ruojnoB"