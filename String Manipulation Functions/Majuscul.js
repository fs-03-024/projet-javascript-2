function mettreMotsEnMajuscule(phrase) {
    const mots = phrase.split(" ");
    const motsEnMajuscule = mots.map((mot) => {
      const premiereLettre = mot.charAt(0).toUpperCase();
      const resteMot = mot.slice(1);
      return premiereLettre + resteMot;
    });
    return motsEnMajuscule.join(" ");
  }
  
  // Exemple d'utilisation
  const phrase = "bonjour tout le monde";
  const phraseEnMajuscule = mettreMotsEnMajuscule(phrase);
  console.log(phraseEnMajuscule); // Affiche "Bonjour Tout Le Monde"