function compterCaracteres(chaine) {
    return chaine.length;
  }
  
  // Exemple d'utilisation
  const chaine = "Bonjour";
  const nombreCaracteres = compterCaracteres(chaine);
  console.log(nombreCaracteres); // Affiche 7