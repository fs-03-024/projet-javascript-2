function filtrerTableau(tableau, condition) {
    const tableauFiltre = [];
  
    for (let i = 0; i < tableau.length; i++) {
      if (condition(tableau[i])) {
        tableauFiltre.push(tableau[i]);
      }
    }
  
    return tableauFiltre;
  }