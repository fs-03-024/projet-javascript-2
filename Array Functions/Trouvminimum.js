function trouverMinimum(tableau) {
    if (tableau.length === 0) {
      return undefined;
    }
  
    let minimum = tableau[0];
  
    for (let i = 1; i < tableau.length; i++) {
      if (tableau[i] < minimum) {
        minimum = tableau[i];
      }
    }
  
    return minimum;
  }