function genererSequenceFibonacci(nombreTermes) {
    const sequence = [0, 1]; // Les deux premiers termes de la séquence
  
    if (nombreTermes <= 2) {
      // Si le nombre de termes demandé est inférieur ou égal à 2,
      // on retourne directement les termes initiaux de la séquence
      return sequence.slice(0, nombreTermes);
    }
  
    for (let i = 2; i < nombreTermes; i++) {
      const terme = sequence[i - 1] + sequence[i - 2];
      sequence.push(terme);
    }
  
    return sequence;
  }