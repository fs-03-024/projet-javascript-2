function estPremier(nombre) {
    if (nombre <= 1) {
      return false;
    }
  
    // Vérification des diviseurs potentiels jusqu'à la racine carrée du nombre
    for (let diviseur = 2; diviseur <= Math.sqrt(nombre); diviseur++) {
      if (nombre % diviseur === 0) {
        return false;
      }
    }
  
    return true;
  }