function calculerFactorielle(nombre) {
    if (nombre === 0) {
      return 1;
    }
  
    let factorielle = 1;
  
    for (let i = 2; i <= nombre; i++) {
      factorielle *= i;
    }
  
    return factorielle;
  }